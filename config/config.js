var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'blogapp'
    },
    port: 3000,
    db: 'mongodb://localhost/blog'
  },

  test: {
    root: rootPath,
    app: {
      name: 'blogapp'
    },
    port: 3000,
    db: 'mongodb://localhost/blogapp-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'blogapp'
    },
    port: 3000,
    db: 'mongodb://localhost/blogapp-production'
  }
};

module.exports = config[env];
