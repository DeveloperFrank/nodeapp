// Example model

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var Post = new Schema({
  title: String,
  content: String,
  created: { type: Date, required: true, default: Date.now}
}, {collection: 'post'});

Post.virtual('date')
  .get(function(){
    return this._id.getTimestamp();
  });

mongoose.model('post', Post);

