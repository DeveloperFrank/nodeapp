"use strict";

var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  Post = mongoose.model('post'),
  ellipsis = require('html-ellipsis');

module.exports = function (app) {
  app.use('/', router);
};

router.get('/', function (req, res, next) {
  Post.find(function (err, post) {
    if (err) return next(err);

    let n = 0;
    let post_r = [];

    for(n = 0; n < post.length; n ++){
      post_r[n] = post[n].content;
      post_r[n] = post_r[n].substring(0, 250);
    }

    res.render('index', {
      title: 'Bloggor',
      post: post,
      post_r: post_r
    });
  }).sort('-created');
});
