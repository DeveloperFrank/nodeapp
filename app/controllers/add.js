var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  Post = mongoose.model('post');

module.exports = function (app) {
  app.use('/', router);
};

router.get('/add', function(req, res, next){
	res.render('add_post');
});

router.post('/add', function(req, res, next){

	var newPost = new Post({
		title: req.body.title,
		content: req.body.content
	});

	newPost.save();
	res.redirect('/');
	res.send(newPost);
});