var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  Post = mongoose.model('post');

module.exports = function (app) {
  app.use('/', router);
};

router.get('/post/:id', function(req, res, next){
	var id_post = req.params.id;

	Post.findById(id_post, function(err, post_view){
		if (err) next(err);
		res.render('post_view', {
			post: post_view
		})
	});
});

router.get('/delete/:id', function(req, res, next) {
	var id_post = req.params.id;

	Post.findByIdAndRemove(id_post, function(err, post){
		res.redirect('/');
	});
});