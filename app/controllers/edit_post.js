var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  Post = mongoose.model('post');

module.exports = function (app) {
  app.use('/', router);
};

var id_post;

 router.get('/edit/:id', function(req, res, next){
 	id_post = req.params.id;
	Post.findById(id_post, function(err, post_find){
		
		//res.json(post_find);
		res.render('edit_post', {
			post: post_find
		});
	});
	
 });

router.put('/edit/:id', function(req, res){
	Post.findById(id_post, function(err, post){

		post.title = req.body.title;
		post.content = req.body.content;
		post.save();
		res.json(post);
	});
});