$(document).ready(function(){

	$('#btn-update').click(function(event) {

		//mozilla firefox fixed, event in function handler event.
		event.preventDefault();

		var id = $(this).attr('id_data');
		var title = $('#title').val();
		var content = $('textarea').val();
		var url = ('/edit/' + id);
		
		//ajax funtion for update data.
		$.ajax({
			type: 'PUT',
			url: url,
			contentType: "application/json",
			// data: $('#formEditPost').serialize(),
			data: JSON.stringify({
				'title': title,
				'content': content
			}),
			dataType: "json",
			success: function (data, status, jqXHR) {
                 console.log("Updating success");
                 window.location.href = '/';
             },
            error: function (jqXHR, status) {
            	alert(status + jqXHR);
                console.log("Update fail");
                console.log(status);
                console.log(jqXHR);
             }
		});
	});

		 // CKEDITOR.replace( 'content', {
		 // 	language: 'es'
		 // });


});
